/**
 * *********************************************************************
 * OpenSWG Sandbox Server
 * Copyright (C) 2006 OpenSWG Team <http://www.openswg.com>
 * *********************************************************************
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * *********************************************************************
 */

#include "SocketServer.h"
#include "ConfigFile.h"
#include "Log.h"
#include "Util.h"

#ifdef WIN32
#define ioctl ioctlsocket
#endif

/** Socket Server constructor
 *	Initializes the Socket Servers data members.
 */
SocketServer::SocketServer()
{
	// Initialize member variables.
	mPort = 0;
	mMode = 0;

    mTimeout.tv_sec = 0;
    mTimeout.tv_usec = 100;
}

/** Initialize Server function
 *	Stores the configuration file and prepares the socket for use.
 */
void SocketServer::InitServer()
{
	InitSocket();

	Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (Socket == INVALID_SOCKET)
	{
		throw new SocketServerException();
	}

	//Set the ioctl mode.
	if(ioctl(Socket, FIONBIO, &mMode ) < 0)
    { //If theres a problem, report it and then close the server.
        sLog.logMessage(LogError, "Ioctl FIBIO (Blocking Mode) failed!");
		throw new SocketServerException();
    }

    memset(&ain, 0, sizeof(ain)); // Clear struct
    ain.sin_family = AF_INET;
    ain.sin_addr.s_addr = INADDR_ANY;
    ain.sin_port = htons(mPort); //sets port

	// Bind the server socket
    if (bind(Socket, (struct sockaddr *)&ain, sizeof(ain)) < 0)
    {
        sLog.logMessage(LogError, "Server Bind Failed!");
		throw new SocketServerException();
    }
}

/** Run Server function
 *	Begins listening on the port specified in the configuration until
 *	the server status is no longer set to running.
 */
void SocketServer::Run()
{
	// Initialize the client length container.
#if WIN32
    int clientlen = sizeof(cclient);
#else
    unsigned int clientlen = sizeof(cclient);
#endif

	// Create a buffer for the received data.
    char recvData[MAX_RECV_SIZE];

	// Create our socket reader.
	fd_set reader;

	do
    {
		mCurrentTime = time(0);
        
		// Initialize the read socket.
		FD_ZERO(&reader);

		// Prepare the socket.
		FD_SET(Socket, &reader);

		// Check for connection attempts.
		if (select(0, &reader, NULL, NULL, &mTimeout) == SOCKET_ERROR)
		{
			sLog.logMessage(LogError, "Lost connection ... attempting graceful shutdown.");
			throw new SocketServerException();
		}

		// If there is incoming data, read and handle it.
		if (FD_ISSET(Socket, &reader))
		{
			// Read the incoming data.
			unsigned short len = 0;
			len = recvfrom(Socket, recvData, MAX_RECV_SIZE, 0, (struct sockaddr*)&cclient, &clientlen);

			// Pass it on to be handled by the appropriate session.
			OnIncoming(cclient, recvData, len);
		}

		OnUpdate();
	}
    while (running); // Loop through until the running state is false.
}

/** Shutdown Server function
 *	Handles any cleanup the server requires before exiting.
 */
void SocketServer::ShutdownServer()
{
#ifdef WIN32
    //closesocket(Socket); //Closes Socket
    WSACleanup(); //The Clean up, not required but makes things much faster.
#endif

    Socket = INVALID_SOCKET; //Prepares it just incase the socket is to be started up again. used to catch errors on startup.
}

/** Add GalaxySession function
 *	Adds a new swg client to the client map.
 */
GalaxySession* SocketServer::AddConnection(struct sockaddr_in address, string ip)
{
	// Create a new session and store it in the session map.
	GalaxySession* session = new GalaxySession(this, address, ip);
	mSessions[ip.c_str()] = session;

	// Return the new session.
	return session;
}

GalaxySession* SocketServer::FindConnection(const std::string& address) const
{
    // Create a new session and store it in the session map.
    GalaxySession* session = nullptr;

    auto find_iter = mSessions.find(address);
    if(find_iter != mSessions.end())
    {
        session = find_iter->second;
    }

    return session;
}

/** On Incoming Data function
 *	Whenever information is received via the socket this function is
 *	called to handle the data.
 */
void SocketServer::OnIncoming(struct sockaddr_in address,
								 char *packet,
								 size_t length)
{
    auto address_str = AddressToString(&address);

    GalaxySession* session = FindConnection(address_str);

    if(!session)
    {
        if(packet[1] == 0x01)
        {
            session = AddConnection(address, address_str);
        }
        else
        {
            sLog.logMessage(LogWarning, "Zone: Got unexpected Opcode [%d], from [%s] invalid client", packet[1], address_str.c_str());
            return;
        }
    }
    
    session->PrepPacket(packet, length);
    session->HandlePacket(packet, length);
}

/** Send Packet function
 *	Sends a packet to the specified to the specified client.
 */
void SocketServer::SendPacket(struct sockaddr_in address, char *packet, unsigned short length)
{
#if WIN32
    int clientlen = sizeof(address);
#else
    unsigned int clientlen = sizeof(address);
#endif
	sendto(Socket, packet, length, 0, (struct sockaddr*)&address, clientlen);
}


/** Init Socket function
 *	Initializes WinSock. This is only called on windows servers.
 */
void SocketServer::InitSocket()
{
#ifdef WIN32
    WSADATA wsa;

	// Fill in the WSADATA struct.
	int error = WSAStartup(0x0202, &wsa);

	// Check for errors.
	if (error)
	{
        sLog.logMessage(LogError, "WSAStartup Failed!");
		throw new SocketServerException();
	}

	// Check WinSock version.
	if (wsa.wVersion != 0x0202)
	{
		WSACleanup();
        sLog.logMessage(LogError, "Wrong WinSock Version Found (v2.2 required)!");
		throw new SocketServerException();
	}
#endif
}
