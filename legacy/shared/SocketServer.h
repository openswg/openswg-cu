/**
 * *********************************************************************
 * OpenSWG Sandbox Server
 * Copyright (C) 2006 OpenSWG Team <http://www.openswg.com>
 * *********************************************************************
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * *********************************************************************
 */

#ifndef OPENSWG_SOCKET_SERVER_H
#define OPENSWG_SOCKET_SERVER_H

#include <array>
#include <map>
#include <memory>
#include <mutex>

#define MAX_RECV_SIZE 512

#ifdef WIN32 //Windows Socket
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <mmsystem.h>
#include <winsock2.h>
#define close closesocket
#else //BSD Socket header files
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <string.h>
#define SOCKET int
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#endif

#include "GalaxySession.h"

extern bool running;

/** SWG Client Map
 *  This is a container for the clients that are currently connected to the server.
 */
class GalaxySession;
typedef std::map<std::string, GalaxySession*> GalaxySessionMap;


/** Socket Server Exception structure
 *  This exception is thrown whenever there is an error that prevents
 *	a method from executing. This allows any issues raised to be handled
 *	in the domain where they are relevant.
 */
struct SocketServerException {};


/** Socket Server class
 *  This class handles the sending and receiving of data from the client.
 */
class SocketServer
{
public:
	/** Socket Server constructor
	 *	Initializes the SWG Socket Servers data members.
	 */
	SocketServer();

	/** Initialize Server function
	 *	Stores the configuration file and prepares the socket for use.
	 */
	virtual void InitServer();

	/** Shutdown Server function
	 *	Handles any cleanup the server requires before exiting.
	 */
	void ShutdownServer();

	/** Run Server function
	 *	Begins listening on the port specified in the configuration until
	 *	the server status is no longer set to running.
	 */
	void Run();

	/** On Incoming Data function
	 *	Whenever information is received via the socket this function is
	 *	called to handle the data.
	 */
	void OnIncoming(struct sockaddr_in, char *packet, size_t length);

	/** Updates the zone state.
	 */
	virtual void OnUpdate() = 0;

	/** Send Packet function
	 *	Sends a packet to the specified to the specified client.
	 */
    void SendPacket(struct sockaddr_in address, char *packet, unsigned short length);

	/** Add New SWG Client function
	 *	Adds a new swg client to the client map.
	 */
	GalaxySession* AddConnection(struct sockaddr_in address, std::string ip);

	GalaxySession* FindConnection(const std::string& address) const;

	int GetPort() { return mPort; }

protected:

	int mPort;
	GalaxySessionMap mSessions;

	SOCKET			Socket;				//Socket
	struct sockaddr_in ain, cclient;

	time_t mCurrentTime;
    struct timeval mTimeout;

	int ret; //Used for blocking modes
	//For setting the blocking mode.
#if WIN32
	unsigned long mMode;
#else
	long mMode;
#endif
    
private:
	void InitSocket();

    std::mutex mMutex;
};

#endif // OPENSWG_SOCKET_SERVER_H
