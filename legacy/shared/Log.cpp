/**
 * *********************************************************************
 * OpenSWG Sandbox Server
 * Copyright (C) 2006 OpenSWG Team <http://www.openswg.com>
 * *********************************************************************
 * Copyright (C) 2005,2006 MaNGOS <http://www.mangosproject.org/>
 * *********************************************************************
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * *********************************************************************
 *
 * This is a customized implementation of the Log class from the mangos
 * project.
 *
 */

#include "Log.h"

#include <ctime>
#include <cstdarg>
#include <chrono>

#include "Util.h"


void Log::Initialize(const std::string& log_filename, LogLevel log_level, LogLevel log_file_level)
{
    if(log_filename != "")
    {
        mLogFileStream.open(log_filename);
    }

    mLogLevel     = log_level;
    mLogFileLevel = log_file_level;
}

void Log::logMessage(LogLevel level, boost::format& message)
{
    std::lock_guard<std::mutex> lg(write_mutex_);

    if(level >= mLogLevel)
    {
        std::cout << message.str() << std::endl;
    }

    if(level >= mLogFileLevel)
    {
        mLogFileStream << createTimestamp() << ": " << message.str() << std::endl;
    }
}
