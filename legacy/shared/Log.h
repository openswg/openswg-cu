// This file is part of CU Dummy which is released under the MIT license.
// See file LICENSE or go to http://openswg.org/LICENSE

#pragma once

#include <cstdint>
#include <fstream>
#include <iosfwd>
#include <mutex>
#include <string>

#include <boost/format.hpp>


enum LogLevel
{
    LogError = 0,
    LogWarning,
    LogInfo,
    LogDebug
};

class Log
{
public:

	static Log& GetInstance()
	{
        static Log instance;
		return instance;
	}

    void Initialize(const std::string& log_filename, LogLevel log_level, LogLevel log_file_level);

    void setConsoleLevel(LogLevel level) { mLogLevel = level; }
    LogLevel getConsoleLevel() const { return mLogLevel; }

    void setFileLevel(LogLevel level) { mLogFileLevel = level; }
    LogLevel getFileLevel() const { return mLogFileLevel; }

    void logMessage(LogLevel level, boost::format& message);

    template<typename ValueT, typename... ArgsT>
    void logMessage(LogLevel level, boost::format& message, ValueT arg, ArgsT... args)
    {
        message % arg;
        logMessage(level, message, std::forward<ArgsT>(args)...);
    }

    template<typename... ArgsT>
    void logMessage(LogLevel level, const std::string& format, ArgsT... args)
    {
        boost::format message(format);
        logMessage(level, message, std::forward<ArgsT>(args)...);
    }

private:
    Log() {}
    ~Log() {}

	LogLevel mLogLevel;
	LogLevel mLogFileLevel;
    std::ofstream mLogFileStream;
    std::mutex write_mutex_;
};

#define sLog Log::GetInstance()
