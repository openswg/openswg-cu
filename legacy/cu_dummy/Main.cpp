/**
 * *********************************************************************
 * OpenSWG Sandbox Server
 * Copyright (C) 2006 OpenSWG Team <http://www.openswg.com>
 * *********************************************************************
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * *********************************************************************
 */

#include <thread>

#include "ConfigFile.h"
#include "Log.h"
#include "LoginServer.h"
#include "ZoneServer.h"

bool running;

#define OPENSWG_CONFIG "openswg.conf"

// Run the OpenSWG Sandbox Server.
int main()
{
	// Attempt to load the configuration file.
	try {
		sConfig.Initialize(OPENSWG_CONFIG);
		sLog.Initialize(sConfig.read<string>("LogFile", ""),
            static_cast<LogLevel>(sConfig.read<int>("LogLevel", 0)),
            static_cast<LogLevel>(sConfig.read<int>("LogFileLevel", 0)));
	}
	catch(...)
	{
		sLog.logMessage(LogError, "Could not find configuration file %s", OPENSWG_CONFIG);
	}

	// Send server header output.
	std::cout << "OpenSWG Combat Upgrade Sandbox v1.0\n";
	std::cout << "Copyright (c) 2006 OpenSWG Development Team\n" << std::endl;

	sLog.logMessage(LogInfo, "Server starting with configuration file: %s", OPENSWG_CONFIG);

	// Begin the main execution phase. This begins by starting the server
	// threads and then running until the exit command is received.
	try {
		// Set the running state.
		running = true;

        std::thread login_thread([] ()
        {
            LoginServer login;
	        try {
	        	login.InitServer();
	        	login.Run();
	        } catch(SocketServerException()) {
	        	sLog.logMessage(LogError, "ERROR: Unable to initialize the login server!");
	        }

	        sLog.logMessage(LogInfo, "Exiting the login thread");
        });

        std::thread zone_thread([] ()
        {
            ZoneServer zone;
	        try {
	        	zone.InitServer();
	        	zone.Run();
	        } catch(SocketServerException()) {
	        	sLog.logMessage(LogError, "ERROR: Unable to initialize the zone server!");
	        }

	        sLog.logMessage(LogInfo, "Exiting the zone thread");
        });

		// This is the command loop. This watches for the incoming commands from
		// the console and updates the server threads accordingly.
		for(;;)
		{
			// Check for a command.
			string cmd;
			std::cin >> cmd;

			// Check for the exit command.
			if (strcmp(cmd.c_str(), "exit") == 0 || strcmp(cmd.c_str(), "quit") == 0 || strcmp(cmd.c_str(), "q") == 0)
			{
				running = false;
				sLog.logMessage(LogInfo, "Shutting down the server");
				break;
			}
            else 
            {
                // Log the invalid command.
			    sLog.logMessage(LogError, "Command not found");
            }
		}

        zone_thread.join();
        login_thread.join();
	}
	catch (std::exception& e)
	{
		sLog.logMessage(LogError, "%s", e.what());
	}

	return 0;
}
