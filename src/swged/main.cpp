
#include <string>
#include <vector>

#include <QApplication>
#include <QTreeView>
#include <QStandardItemModel>

#include <boost/algorithm/string.hpp>

#include "cuswg/resource/tre/tre_archive.h"
#include "main_window.h"

std::unique_ptr<QStandardItemModel> BuildTreModel(cuswg::TreArchive& archive);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    
    MainWindow mw;
    mw.show();

    //cuswg::TreArchive archive("D:/SWG/swgdata/base/live.cfg");
    //
    //auto model = BuildTreModel(archive);
    //
    //QTreeView view;
    //view.setModel(model.get());
    //view.setWindowTitle(QObject::tr("Simple Tree Model"));
    //view.show();
    //
    return a.exec();
}


std::unique_ptr<QStandardItemModel> BuildTreModel(cuswg::TreArchive& archive)
{
    std::map<std::string, QStandardItem*> item_cache;
    std::unique_ptr<QStandardItemModel> model(new QStandardItemModel());
    auto file_listing = archive.GetAvailableResources();

    int top_level_count = 0;
    for (const auto& file : file_listing) {
        std::vector<std::string> path_data;
        std::string current_depth;
        boost::split(path_data, file, boost::is_any_of("/"));

        QStandardItem* previous = nullptr;

        auto path_depth = path_data.size();
        for (uint32_t i = 0; i < path_depth; ++i) {
            const auto& current_item = path_data[i];
            current_depth += "/" + current_item;
            QStandardItem* item = nullptr;

            if (i + 1 == path_depth) {
                // This the file element (last loop iteration)
                item = new QStandardItem(QString::fromStdString(current_item));
                item->setEditable(false);
                
                if (previous) {
                    previous->appendRow(item);
                }
            } else {            
                auto find_iter = item_cache.find(current_depth);
                if (find_iter != item_cache.end()) {
                    // This is a directory that's already been cached
                    previous = find_iter->second;
                } else {
                    // This is a directory that has not been cached yet                
                    item = new QStandardItem(QString::fromStdString(current_item));
                    item->setEditable(false);

                    if (previous) {
                        previous->appendRow(item);
                    }
                    
                    previous = item;

                    item_cache.insert(std::make_pair(current_depth, item));
                }
            }

            if (i == 0 && item) {
                model->setItem(top_level_count++, 0, item);
            }
        }
    }

    return model;
}