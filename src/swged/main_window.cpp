
#include "main_window.h"
#include "ui_main_window.h"

//! [0]
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
//! [0]

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
