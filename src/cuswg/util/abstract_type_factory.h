
#pragma once

#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>

#include <boost/noncopyable.hpp>

namespace cuswg {

struct TypeRegistration {
    typedef std::function<void* ()> Creator;
    typedef std::function<void (void*)> Destroyer;

    Creator creator;
    Destroyer destroyer;
};

class AbstractTypeFactory : boost::noncopyable {
public:
    typedef std::invalid_argument InvalidType;

    void registerType(const std::string& name, TypeRegistration registration);

    template<typename T>
    std::shared_ptr<T> createType(const std::string& name) const {
        if (auto object = createAbstractType(name)) {
            return std::static_pointer_cast<T>(object);
        }

        return nullptr;
    }

    template<typename T, typename... ArgsT>
    std::shared_ptr<T> createType(const std::string& name, ArgsT&&... args) const {
        auto object = createType<T>(name);

        if (object) {
            object->initialize(std::forward<ArgsT>(args)...);
        }

        return object;
    }

private:
    std::shared_ptr<void> createAbstractType(const std::string& name) const;

    std::map<std::string, TypeRegistration> registered_types_;
};


}  // namespace cuswg
