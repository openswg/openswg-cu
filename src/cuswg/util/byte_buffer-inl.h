
#pragma once

#include <algorithm>
#include <string>
#include <stdexcept>

namespace cuswg {

template<typename T>
ByteBuffer& ByteBuffer::write(T data) {
  write(reinterpret_cast<unsigned char*>(&data), sizeof(T));
  return *this;
}

template<typename T>
ByteBuffer& ByteBuffer::write(std::vector<T> vec) {
    std::for_each(vec.begin(), vec.end(), [this] (T data)
    {
        this->write(data);
    });
    return *this;
}

template<typename T>
ByteBuffer& ByteBuffer::writeAt(size_t offset, T data) {
  write(offset, reinterpret_cast<unsigned char*>(&data), sizeof(T));
  return *this;
}

template<typename T>
const T ByteBuffer::read(bool /*null_terminated*/) const {
  T data = peek<T>();
  read_position_ += sizeof(T);
  return data;
}

template<typename T>
const T ByteBuffer::peek() const {
  return peekAt<T>(read_position_);
}

template<typename T>
const T ByteBuffer::peekAt(size_t offset) const {
  if (data_.size() < offset + sizeof(T)) {
    throw std::out_of_range("Read past end of buffer");
  }

  T data = *reinterpret_cast<const T*>(&data_[offset]);

  return data;
}

template<> ByteBuffer& ByteBuffer::write<std::string>(std::string data);
template<> const std::string ByteBuffer::read<std::string>(bool null_terminated) const;
template<> ByteBuffer& ByteBuffer::write<std::wstring>(std::wstring data);
template<> const std::wstring ByteBuffer::read<std::wstring>(bool null_terminated) const;

template<typename T>
ByteBuffer& operator<<(ByteBuffer& buffer, const T& value) {
  buffer.write<T>(value);
  return buffer;
}

}  // namespace cuswg
