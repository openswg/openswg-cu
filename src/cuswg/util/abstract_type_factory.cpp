
#include "abstract_type_factory.h"

using namespace cuswg;

void AbstractTypeFactory::registerType(const std::string& name, TypeRegistration registration) {
    if (name.empty()) {
        throw InvalidType("Type name cannot be empty");
    }

    if (registered_types_.find(name) != registered_types_.end()) {
        throw InvalidType("Type registration already exists for: " + name);
    }

    registered_types_.insert(std::make_pair(name, std::move(registration)));
}

std::shared_ptr<void> AbstractTypeFactory::createAbstractType(const std::string& name) const {
    auto find_iter = registered_types_.find(name);
    if (find_iter == registered_types_.end()) {
        throw InvalidType("No type registered by this name: " + name);
    }

    auto& reg = find_iter->second;
    return std::shared_ptr<void>(reg.creator(), reg.destroyer);
}
