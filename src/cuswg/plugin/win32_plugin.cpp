
#ifdef WIN32

#include "win32_plugin.h"

#include <exception>

#include <boost/filesystem.hpp>

using namespace cuswg;

namespace bf = boost::filesystem;

Win32Plugin::Win32Plugin(const std::string& path)
    : handle_(nullptr)
    , exit_func_(0) {
    bf::path library_path(path);

    handle_ = ::LoadLibrary(library_path.string().c_str());

    if (handle_ == nullptr) {
      throw std::runtime_error("Unable to open win32 library: " + path + ", error: " + ::GetLastError());
    }
}

Win32Plugin::~Win32Plugin() {
    if (handle_) {
        ::FreeLibrary(handle_);
    }
}

void * Win32Plugin::getSymbol(const string& symbol) {
    if (!handle_) {
        return nullptr;
    }
    return ::GetProcAddress(handle_, symbol.c_str());
}

#endif  // WIN32
