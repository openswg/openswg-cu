
#pragma once

#include <cstdint>
#include <string>

namespace cuswg {

class AbstractTypeFactory;

typedef void (*ExitFunc)();
typedef ExitFunc (*InitFunc)(AbstractTypeFactory*);

}  // namespace cuswg

#ifdef WIN32
    #ifdef DLL_EXPORTS
        #define PLUGIN_API __declspec(dllexport)
    #else
        #define PLUGIN_API __declspec(dllimport)
    #endif
#else
    #define PLUGIN_API
#endif
