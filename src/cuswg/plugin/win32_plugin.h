
#pragma once

#ifdef WIN32

#include <string>

#include <Windows.h>

#include "plugin_bindings.h"
#include "plugin_interface.h"

namespace cuswg {

class Win32Plugin : public PluginInterface {
public:
    explicit Win32Library(const std::string& path);
    ~Win32Plugin();

    void* getSymbol(const std::string& symbol);

private:
    Win32Library();

private:
    HMODULE handle_;
    ExitFunc exit_func_;
};

}  // namespace cuswg

#endif  // WIN32
