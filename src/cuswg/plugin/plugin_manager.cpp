
#include "plugin_manager.h"

#include <boost/filesystem.hpp>

using namespace cuswg;

namespace bf = boost::filesystem;

#ifdef WIN32
#include "win32_plugin.h"
typedef Win32Plugin PlatformPlugin;
#else
#include <dlfcn.h>
#include "posix_plugin.h"
typedef PosixPlugin PlatformPlugin;
#endif

#if defined(WIN32)
  static std::string library_extension(".dll");
#else
  static std::string library_extension(".so");
#endif

PluginManager::PluginManager(AbstractTypeFactory* type_factory)
    : type_factory_(type_factory)
{
#ifndef WIN32
    dlopen(NULL,RTLD_NOW|RTLD_GLOBAL);
#endif
}

PluginManager::~PluginManager() {}

void PluginManager::loadPlugin(const std::string& plugin_path) {
    if (libraries_.find(plugin_path) != libraries_.end()) {
        throw PluginLoadError("Plugin has already been loaded: " + plugin_path);
    }

    libraries_.insert(std::make_pair(plugin_path, load(plugin_path)));
}

void PluginManager::loadPluginDir(const std::string& directory) {
    bf::path plugin_dir(directory);

    if (!bf::exists(plugin_dir) || !bf::is_directory(plugin_dir)) {
        throw PluginLoadError("Invalid plugin directory: " + directory);
    }

    std::for_each(bf::directory_iterator(plugin_dir), bf::directory_iterator(),
        [this] (const bf::directory_entry& e) {
            if (e.path().extension().compare(library_extension) != 0) return;

            loadPlugin(e.path().native());
        });
}

std::unique_ptr<PluginInterface> PluginManager::load(const std::string& path) {
    return std::unique_ptr<PlatformPlugin>(new PlatformPlugin(path));
}
