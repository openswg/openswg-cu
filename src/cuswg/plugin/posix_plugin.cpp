
#ifndef WIN32

#include "posix_plugin.h"

#include <exception>

#include <dlfcn.h>

#include <boost/filesystem.hpp>

using namespace cuswg;

namespace bf = boost::filesystem;

PosixPlugin::PosixPlugin(const std::string& path)
    : handle_(nullptr)
    , exit_func_(0) {
    bf::path library_path(path);

    void* handle = dlopen(library_path.c_str(), RTLD_LAZY | RTLD_GLOBAL);

    if (handle == nullptr) {
        throw std::runtime_error("Unable to open posix library: " + path + ", error: " + dlerror());
    }
}

PosixPlugin::~PosixPlugin() {
    if (handle_) {
        dlclose(handle_);
    }
}

void* PosixPlugin::getSymbol(const std::string& symbol) {
    if (!handle_) {
        return nullptr;
    }

    return dlsym(handle_, symbol.c_str());
}

#endif  // WIN32
