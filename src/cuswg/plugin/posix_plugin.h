
#pragma once

#ifndef WIN32

#include <string>

#include "plugin_bindings.h"
#include "plugin_interface.h"

namespace cuswg {

class PosixPlugin : public PluginInterface {
public:
    explicit PosixPlugin(const std::string& path);
    ~PosixPlugin();

    void* getSymbol(const std::string& symbol);

private:
    PosixPlugin();

    void* handle_;
    ExitFunc exit_func_;
};

}  // namespace cuswg

#endif
