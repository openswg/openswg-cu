
#pragma once

#include <cstdint>
#include <exception>
#include <map>
#include <memory>
#include <string>

#include "plugin_bindings.h"

namespace cuswg {

class AbstractTypeFactory;
class PluginInterface;

typedef std::runtime_error MissingSymbolError;
typedef std::runtime_error PluginLoadError;

class PluginManager {
public:
    explicit PluginManager(AbstractTypeFactory* type_factory);
    ~PluginManager();

    void loadPlugin(const std::string& plugin_path);
    void loadPluginDir(const std::string& directory);

private:
    PluginManager();

    std::unique_ptr<PluginInterface> load(const std::string& path);

    AbstractTypeFactory* type_factory_;
    std::map<std::string, std::unique_ptr<PluginInterface>> libraries_;
};

}  // namespace cuswg
