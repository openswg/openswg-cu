
#pragma once

#include <string>
#include <boost/noncopyable.hpp>

namespace cuswg {

class PluginInterface : boost::noncopyable {
public:
    virtual ~PluginInterface() {}

    template<typename T>
    T getSymbol(const std::string& symbol) {
        T func_ptr;
        *reinterpret_cast<void**>(&func_ptr) = getSymbol(symbol);
        return func_ptr;
    }

    virtual void* getSymbol(const std::string& symbol) = 0;
};

}  // namespace cuswg
