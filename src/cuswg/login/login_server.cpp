
#include "login_server.h"

#include <zlib.h>

#include "cuswg/util/crc.h"
#include "cuswg/util/utilities.h"

using namespace cuswg;

LoginServer::LoginServer(boost::asio::io_service& io)
    : BaseSoeServer(io)
{}

void LoginServer::handleGameMessage(Connection* connection, ByteBuffer message)
{
    std::cout << "Handling login message" << std::endl;
    std::cout << message << std::endl;
}
