
#pragma once

#include "cuswg/network/base_soe_server.h"

namespace cuswg {

class LoginServer : public BaseSoeServer
{
public:
    explicit LoginServer(boost::asio::io_service& io);

private:
    void handleGameMessage(Connection* connection, ByteBuffer message);
};

}  // namespace cuswg
