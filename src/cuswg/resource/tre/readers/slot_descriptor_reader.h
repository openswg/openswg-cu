
#pragma once

#include <memory>
#include <string>
#include <vector>

#include "cuswg/resource/tre/iff_reader.h"

namespace cuswg {
    
class ResourceHandle;
class SlotDescriptorReader
{
public:
    explicit SlotDescriptorReader(const std::shared_ptr<ResourceHandle>& resource);

    ~SlotDescriptorReader();

    const std::vector<std::string>& GetSlots() const;

private:
    SlotDescriptorReader();

    void LoadSlotsFromNode_(IffReader::Node* node);

    std::vector<std::string> slots_;
    IffReader iff_reader_;
};

}  // namespace cuswg
