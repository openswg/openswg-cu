
#pragma once

#include <memory>
#include <string>
#include <vector>

#include "cuswg/resource/tre/iff_reader.h"

namespace cuswg {    
    
class ResourceHandle;
class SlotArrangementReader
{
public:
    typedef std::vector<std::string> SlotList;

    explicit SlotArrangementReader(const std::shared_ptr<ResourceHandle>& resource);

    ~SlotArrangementReader();

    const std::vector<SlotList>& GetArrangement() const;

private:
    SlotArrangementReader();

    void LoadArranagementFromNodes_(std::list<IffReader::Node*> nodes);
    SlotList LoadSlotsFromNode_(IffReader::Node* node);

    std::vector<SlotList> arrangement_;
    IffReader iff_reader_;
};

}  // namespace cuswg
