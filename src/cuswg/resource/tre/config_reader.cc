
#include "config_reader.h"

#include <array>
#include <fstream>
#include <stdexcept>

#ifdef HAS_STD_REGEX
#include <regex>
using std::regex;
using std::smatch;
using std::regex_match;
#else
#include <boost/regex.hpp>
using boost::regex;
using boost::smatch;
using boost::regex_match;
#endif

#include <boost/filesystem.hpp>

using namespace cuswg;

using std::ifstream;
using std::ios_base;
using std::move;
using std::invalid_argument;
using std::string;
using std::vector;

namespace bfs = boost::filesystem;

ConfigReader::ConfigReader(std::string filename)
    : config_filename_(move(filename))
{
    ParseConfig();
}

const std::vector<std::string>& ConfigReader::GetTreFilenames()
{
    return tre_filenames_;
}

void ConfigReader::ParseConfig()
{
    ifstream input_stream(config_filename_.c_str());

    if (!input_stream.is_open())
    {
        throw invalid_argument("Invalid tre configuration file: " + config_filename_);
    }

    bfs::path p(config_filename_);
    bfs::path dir = p.parent_path();

#ifdef WIN32
    regex rx("searchTree_([0-9]{2})_([0-9]{1,2})=(\")?(.*)\\3");
#else
    regex rx("searchTree_([0-9]{2})_([0-9]{1,2})=(\")?(.*)(?(3)\\3|)");
#endif
    smatch match;
    string line;

    while(!input_stream.eof())
    {
        Getline(input_stream, line);
        if (regex_search(line, match, rx))
        {
            bfs::path tmp = dir;
            bfs::path filename = match[4].str();

            if (!bfs::is_regular_file(filename))
            {
                tmp /= filename;
                filename = tmp;
            }

            auto native_path = bfs::system_complete(filename).native();
            tre_filenames_.emplace_back(std::begin(native_path), std::end(native_path));
        }
    }
}


std::istream& ConfigReader::Getline(std::istream& input, std::string& output)
{
    char c = 0;

    output.clear();

    while(input.get(c) && c != '\n' && c != '\r')
    {
        output += c;
    }

    return input;
}
