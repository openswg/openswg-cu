
#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace cuswg {

class ResourceArchiveInterface
{
public:
	virtual ~ResourceArchiveInterface() {}

	virtual bool Open() = 0;
	virtual uint32_t GetResourceSize(const std::string& resource_name) const = 0;
	virtual void GetResource(const std::string& resource_name, std::vector<char>& buffer) = 0;
};

}  // namespace cuswg
