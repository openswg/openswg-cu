
#pragma once

#include <cstdint>
#include <functional>
#include <vector>
#include <boost/asio.hpp>

#include "cuswg/util/byte_buffer.h"

namespace cuswg {

class UdpSocket
{
public:
    typedef boost::asio::ip::udp::socket Socket;
    typedef boost::asio::ip::udp::endpoint Endpoint;
    typedef std::vector<uint8_t> Buffer;
    typedef std::function<void (ByteBuffer, Endpoint)> ReceiveHandler;

    explicit UdpSocket(boost::asio::io_service& io,
        uint32_t max_receive_size = 496);

    void startListening(const Endpoint& listen_endpoint, ReceiveHandler&& handler);
    void stopListening();

    void sendTo(const ByteBuffer& send_buffer, const Endpoint& endpoint);

    uint32_t getMaxReceiveSize() const { return max_receive_size_; }
    uint64_t getBytesReceived() const { return bytes_received_; }
    uint64_t getBytesSent() const { return bytes_sent_; }
    const Endpoint& getServerEndpoint() const { return listen_endpoint_; }

private:
    UdpSocket();

    void asyncReceiveLoop();

    uint64_t bytes_received_;
    uint64_t bytes_sent_;
    uint32_t max_receive_size_;
    Endpoint listen_endpoint_;

    Endpoint receive_endpoint_;
    Buffer receive_buffer_;

    Socket socket_;
    ReceiveHandler receive_handler_;
};

}  // namespace cuswg
