
#pragma once

#include <cstdint>
#include <functional>
#include <list>
#include <tuple>

#include <boost/optional.hpp>

#include "udp_socket.h"

namespace cuswg {

class Connection
{
public:
    typedef std::function<void (uint16_t)> SequencedCallback;
    typedef std::list<std::tuple<uint16_t, ByteBuffer, boost::optional<SequencedCallback>>> MessageList;

    explicit Connection(const UdpSocket::Endpoint& endpoint)
        : connection_id_(0)
        , crc_length_(2)
        , crc_seed_(0xdeadbabe)
        , next_client_sequence_(0)
        , server_sequence_(0)
        , endpoint_(endpoint)
    {}

    bool isConnected() const { return connection_id_ != 0; }
    
    uint32_t getConnectionId() const { return connection_id_; }
    uint32_t getReceiveBufferSize() const { return receive_buffer_size_; }
    uint32_t getCrcLength() const { return crc_length_; }
    uint32_t getCrcSeed() const { return crc_seed_; }
    uint16_t getNextClientSequence() const { return next_client_sequence_; }
    uint16_t getServerSequence() const { return server_sequence_; }
    uint32_t getMaxDataSize() const { return receive_buffer_size_ - crc_length_ - 3; }
    
    void setConnectionId(uint32_t connection_id) { connection_id_ = connection_id; }
    void setReceiveBufferSize(uint32_t buffer_size) { receive_buffer_size_ = buffer_size; }
    void setCrcLength(uint32_t crc_length) { crc_length_ = crc_length; }
    void setCrcSeed(uint32_t crc_seed) { crc_seed_ = crc_seed; }
    void setNextClientSequence(uint16_t sequence) { next_client_sequence_ = sequence; }

    const UdpSocket::Endpoint& getEndpoint() const { return endpoint_; }
    
    MessageList& getUnacknowledgedMessages() { return unacknowledged_messages_; }

    ByteBuffer& getMessageFragments() { return message_fragments_; }

    uint16_t getFragmentTotalLength() const { return fragment_total_len_; }

    void setFragmentTotalLength(uint16_t length) { fragment_total_len_ = length; }

    uint16_t incrementServerSequence() { return server_sequence_++; }

    void addUnacknowledgedMessage(uint16_t sequence, ByteBuffer message, boost::optional<SequencedCallback> callback)
    {
        unacknowledged_messages_.emplace_back(std::make_tuple(sequence, std::move(message), std::move(callback)));
    }

    void acknowledgeMessage(uint16_t sequence)
    {
        while(std::get<0>(unacknowledged_messages_.front()) <= sequence) {
            (*std::get<2>(unacknowledged_messages_.front()))(sequence);
            unacknowledged_messages_.pop_front();
        }
    }

private:

    uint32_t connection_id_;
    uint32_t receive_buffer_size_;
    uint32_t crc_length_;
    uint32_t crc_seed_;
    uint16_t next_client_sequence_;
    uint16_t server_sequence_;

    MessageList unacknowledged_messages_;
    ByteBuffer message_fragments_;
    uint16_t fragment_total_len_;
    UdpSocket::Endpoint endpoint_;
};

}  // namespace cuswg
