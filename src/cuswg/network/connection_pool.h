
#pragma once

#include <map>
#include <memory>

#include "connection.h"
#include "udp_socket.h"

namespace cuswg {

class ConnectionPool
{
public:
    Connection* find(const UdpSocket::Endpoint& endpoint) const;
    Connection* add(const UdpSocket::Endpoint& endpoint);
    bool remove(const UdpSocket::Endpoint& endpoint);
    bool remove(Connection* connection);

private:
    typedef std::map<UdpSocket::Endpoint, std::unique_ptr<Connection>> ConnectionMap;

    ConnectionMap connections_;
};

}  // namespace cuswg
