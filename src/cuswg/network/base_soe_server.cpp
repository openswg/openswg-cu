
#include "base_soe_server.h"

#include <zlib.h>

#include "cuswg/util/crc.h"
#include "cuswg/util/utilities.h"

using namespace cuswg;

BaseSoeServer::BaseSoeServer(boost::asio::io_service& io)
    : io_strand_(io)
    , socket_(io)
    , compression_buffer_(socket_.getMaxReceiveSize())
    , decompression_buffer_(socket_.getMaxReceiveSize())
{}

void BaseSoeServer::startListening(uint16_t listen_port)
{
    socket_.startListening(UdpSocket::Endpoint(boost::asio::ip::udp::v4(), listen_port), [this] (ByteBuffer message, UdpSocket::Endpoint remote_endpoint)
    {
        try {
            if(auto conn = connections_.find(remote_endpoint)) {
                testCrc(conn, message);
                decrypt(conn, message);
                decompress(conn, message);

                if(message.peek<uint8_t>() != 0) {
                    handleGameMessage(conn, std::move(message));
                } else {
                    handleProtocolMessage(conn, std::move(message));
                }
            } else {
                handleSessionRequest(remote_endpoint, message);
            }
        } catch(std::exception& e) {
            std::cout << "Error processing message: " << e.what() << std::endl;
            std::cout << message << std::endl;
            return;
        }
    });
}

void BaseSoeServer::stopListening()
{
    socket_.stopListening();
}

void BaseSoeServer::sendMessage(Connection* connection, ByteBuffer& message, boost::optional<Connection::SequencedCallback> callback)
{
    if(message.size() > connection->getMaxDataSize()) {
        sendFragmentedMessage(connection, message, std::move(callback));
    } else {
        sendSequencedMessage(connection, CHILD_DATA_A, message, std::move(callback));
    }
}

ByteBuffer BaseSoeServer::sendProtocolMessage(Connection* connection, ByteBuffer message)
{
    compress(connection, message);
    encrypt(connection, message);
    addCrc(connection, message);

    socket_.sendTo(message, connection->getEndpoint());

    return message;
}

void BaseSoeServer::sendFragmentedMessage(Connection* connection, ByteBuffer& message, boost::optional<Connection::SequencedCallback> callback)
{
    auto fragments = splitDataChannelMessage(std::move(message), connection->getMaxDataSize());
    while(fragments.size() > 0) {
        auto& fragment = fragments.front();

        if(fragments.size() == 1) {
            sendSequencedMessage(connection, DATA_FRAG_A, fragment, std::move(callback));
        } else {
            sendSequencedMessage(connection, DATA_FRAG_A, fragment, boost::optional<Connection::SequencedCallback>());
        }

        fragments.pop_front();
    }
}
void BaseSoeServer::sendSequencedMessage(Connection* connection, ProtocolOpcodes header, ByteBuffer& message, boost::optional<Connection::SequencedCallback> callback)
{
    auto message_sequence = connection->incrementServerSequence();

    ByteBuffer message_buffer(message.size() + 4);
    message_buffer.write(hostToBig<uint16_t>(header));
    message_buffer.write(hostToBig(message_sequence));
    message_buffer.append(message);

    message_buffer = sendProtocolMessage(connection, message_buffer);

    connection->addUnacknowledgedMessage(message_sequence, message, std::move(callback));
}

void BaseSoeServer::disconnect(Connection* connection)
{
    if(connection->isConnected()) {
        sendDisconnect(connection);
    }

    connection->setConnectionId(0);
    connections_.remove(connection);
}

void BaseSoeServer::handleProtocolMessage(Connection* connection, ByteBuffer message)
{
    switch(bigToHost(message.peek<uint16_t>()))
    {
        case CHILD_DATA_A:	   { handleChildDataA(connection, deserialize<ChildDataA>(message)); break; }
        case MULTI_PACKET:	   { handleMultiPacket(connection, deserialize<MultiPacket>(message)); break; }
        case DATA_FRAG_A:	   { handleDataFragA(connection, deserialize<DataFragA>(message)); break; }
        case ACK_A:			   { handleAckA(connection, deserialize<AckA>(message)); break; }
        case NET_STATS_CLIENT: { handleNetStatsClient(connection, deserialize<NetStatsClient>(message)); break; }
        case OUT_OF_ORDER_A:   { handleOutOfOrderA(connection, deserialize<OutOfOrderA>(message)); break; }
        case PING:			   { handlePing(connection, deserialize<Ping>(message)); break; }
        case DISCONNECT:	   { disconnect(connection); break; }
        case FATAL_ERROR:      { disconnect(connection); break; }
        default:
            {
                std::cout << "Received invalid message from connected client: " << connection->getEndpoint() << std::endl;
                std::cout << message << std::endl;
            }
    }
}

void BaseSoeServer::handleSessionRequest(UdpSocket::Endpoint remote_endpoint, ByteBuffer& message)
{
    auto request = deserialize<SessionRequest>(message);

    auto connection = connections_.add(remote_endpoint);
    connection->setConnectionId(request.connection_id);
    connection->setReceiveBufferSize(request.client_udp_buffer_size);
    connection->setCrcLength(request.crc_length);

    sendSessionResponse(connection);

    std::cout << "Created Session [" << connection->getConnectionId() << "] @ " << connection->getEndpoint() << std::endl;
}

void BaseSoeServer::handleMultiPacket(Connection* connection, MultiPacket packet)
{
    for(auto message : packet.messages) {
        handleProtocolMessage(connection, message);
    }
}

void BaseSoeServer::handlePing(Connection* connection, Ping packet)
{
    Ping ping;
    sendProtocolMessage(connection, serialize(ping));
}

void BaseSoeServer::handleNetStatsClient(Connection* connection, NetStatsClient packet)
{
    auto stats = server_net_stats_;
    stats.client_tick_count = packet.client_tick_count;

    sendProtocolMessage(connection, serialize(stats));
}

void BaseSoeServer::handleChildDataA(Connection* connection, ChildDataA packet)
{
    if(acknowledgeSequence(connection, packet.sequence)) {
        for(const auto& message : packet.messages) {
            handleGameMessage(connection, message);
        }
    }
}

void BaseSoeServer::handleDataFragA(Connection* connection, DataFragA packet)
{
    if(acknowledgeSequence(connection, packet.sequence)) {
        auto& fragments = connection->getMessageFragments();

        if(fragments.size() == 0) {
            connection->setFragmentTotalLength(packet.data.read<uint16_t>());
        }

        fragments.append(packet.data);

        if(connection->getFragmentTotalLength() == fragments.size()) {
            ChildDataA data_message;
            data_message.messages.push_back(fragments);

            handleChildDataA(connection, data_message);
            fragments.clear();
        }
    }
}

void BaseSoeServer::handleAckA(Connection* connection, AckA packet)
{
    connection->acknowledgeMessage(packet.sequence);
}

void BaseSoeServer::handleOutOfOrderA(Connection* connection, OutOfOrderA packet)
{
    for(const auto& message : connection->getUnacknowledgedMessages()) {
        socket_.sendTo(std::get<1>(message), connection->getEndpoint());

        if(std::get<0>(message) == packet.sequence) break;
    }
}

void BaseSoeServer::sendSessionResponse(Connection* connection)
{
    SessionResponse session_response;
    session_response.connection_id = connection->getConnectionId();
    session_response.crc_seed = connection->getCrcSeed();
    session_response.crc_length = connection->getCrcLength();
    session_response.encryption_type = 1;
    session_response.server_udp_buffer_size = socket_.getMaxReceiveSize();

    socket_.sendTo(serialize(session_response), connection->getEndpoint());
}

void BaseSoeServer::sendDisconnect(Connection* connection, uint16_t reason)
{
    Disconnect disconnect;
    disconnect.connection_id = connection->getConnectionId();
    disconnect.reason_id = reason;

    sendProtocolMessage(connection, serialize(disconnect));
}

void BaseSoeServer::sendAck(Connection* connection, uint16_t sequence)
{
    AckA ack;
    ack.sequence = sequence;

    sendProtocolMessage(connection, serialize(ack));
}

void BaseSoeServer::sendOutOfOrder(Connection* connection, uint16_t sequence)
{
    OutOfOrderA oo;
    oo.sequence = sequence;

    sendProtocolMessage(connection, serialize(oo));
}

bool BaseSoeServer::acknowledgeSequence(Connection* connection, uint16_t sequence)
{
    uint16_t next_client_sequence = connection->getNextClientSequence();

    if(next_client_sequence == sequence) {
        connection->setNextClientSequence(sequence + 1);
        sendAck(connection, sequence);

        return true;
    }

    if(sequence > (next_client_sequence + 50)) {
		disconnect(connection);
	} else {
        sendOutOfOrder(connection, next_client_sequence - 1);
    }

	return false;
}

void BaseSoeServer::compress(Connection* connection, ByteBuffer& message)
{
    if(message.size() > connection->getReceiveBufferSize() - 20) {
        // Determine the offset to begin compressing data at
        uint8_t offset = (message.peekAt<uint8_t>(0) == 0x00) ? 2 : 1;
        uLong output_length = compression_buffer_.capacity();

        ::compress(reinterpret_cast<Bytef*>(&compression_buffer_[0]), &output_length,
            reinterpret_cast<Bytef*>(message.data(offset)), message.size() - offset);

        message.write_position(offset);
        message.write(&compression_buffer_[0], output_length);
        message.resize(output_length + offset);
        message.write<uint8_t>(0x01);
    } else {
        message.write<uint8_t>(0x00);
    }
}

void BaseSoeServer::decompress(Connection* connection, ByteBuffer& message)
{
    uint32_t size_without_compression_bit = message.size() - 1;
    uint8_t compressed_bit = message.peekAt<uint8_t>(size_without_compression_bit);

    message.resize(size_without_compression_bit);

    if(compressed_bit == 1) {
        uint8_t offset = (message.peekAt<uint8_t>(0) == 0x00) ? 2 : 1;
        uLong output_length = decompression_buffer_.capacity();

        ::uncompress(reinterpret_cast<Bytef*>(&decompression_buffer_[0]), &output_length,
            reinterpret_cast<Bytef*>(message.data(offset)), message.size() - offset);

        message.write_position(offset);
        message.write(&decompression_buffer_[0], output_length);
        message.resize(output_length + offset);
    }
}

void BaseSoeServer::decrypt(Connection* connection, ByteBuffer& message)
{
    uint8_t offset = (message.peekAt<uint8_t>(0) == 0x00) ? 2 : 1;
    uint32_t seed = connection->getCrcSeed();
    uint32_t length = message.size() - offset;
    uint32_t temp_seed = 0;
    uint32_t block_count = (length / 4);

    auto raw_data = message.data(offset);
    auto block_data = reinterpret_cast<uint32_t*>(raw_data);

    for(uint32_t count = 0; count < block_count; ++count) {
        temp_seed = block_data[count];
        block_data[count] ^= seed;
        seed = temp_seed;
    }

    for(uint32_t count = block_count * 4; count < length; ++count) {
        raw_data[count] ^= seed;
    }
}

void BaseSoeServer::encrypt(Connection* connection, ByteBuffer& message)
{
    uint8_t offset = (message.peekAt<uint8_t>(0) == 0x00) ? 2 : 1;
    uint32_t seed = connection->getCrcSeed();
    uint32_t length = message.size() - offset;
    uint32_t block_count = (length / 4);

    auto raw_data = message.data(offset);
    auto block_data = reinterpret_cast<uint32_t*>(raw_data);

    for(uint32_t count = 0; count < block_count; ++count) {
        block_data[count] ^= seed;
        seed = block_data[count];
    }

    for(uint32_t count = block_count * 4; count < length; count++) {
        raw_data[count] ^= seed;
    }
}

void BaseSoeServer::testCrc(Connection* connection, ByteBuffer& message)
{
    uint32_t crc_length = connection->getCrcLength();

    if (crc_length == 0) return;

    // Peel off the crc bits from the packet data.
    uint32_t message_size = message.size() - crc_length;
    std::vector<uint8_t> crc_bits(message.c_data() + message_size, message.c_data() + message.size());
    message.resize(message_size);

    uint32_t packet_crc = memcrc(message.c_data(), message.size(), connection->getCrcSeed());
    uint32_t test_crc = 0;
    uint32_t mask = 0;

    for (uint8_t i = 0; i < crc_length; ++i) {
        test_crc |= (crc_bits[i] << (((crc_length - 1) -  i) * 8));
        mask <<= 8;
        mask |= 0xFF;
    }

    packet_crc &= mask;

    if (test_crc != packet_crc) throw std::runtime_error("Crc mismatch");
}

void BaseSoeServer::addCrc(Connection* connection, ByteBuffer& message)
{
    uint32_t crc_length = connection->getCrcLength();

    if (crc_length == 0) return;

    uint32_t packet_crc = memcrc(message.c_data(), message.size(), connection->getCrcSeed());

    uint8_t crc_low = (uint8_t)packet_crc;
    uint8_t crc_high = (uint8_t)(packet_crc >> 8);

    message << crc_high << crc_low;
}

std::list<ByteBuffer> BaseSoeServer::splitDataChannelMessage(ByteBuffer message, uint32_t max_size)
{
    uint32_t message_size = message.size();

    if (message_size < max_size) {
        throw std::invalid_argument("Message must be bigger than max size");
    }

    std::list<ByteBuffer> fragmented_messages;

    uint32_t remaining_bytes = message_size;
    uint32_t chunk_size = 0;

    while (remaining_bytes != 0) {
        ByteBuffer fragment;
        chunk_size = (remaining_bytes > max_size) ? max_size : remaining_bytes;

        if (remaining_bytes == message_size) {
            chunk_size -= sizeof(uint32_t);
            fragment.write<uint32_t>(hostToBig<uint32_t>(message_size));
        }

        fragment.write(message.data() + (message_size - remaining_bytes), chunk_size);

        fragmented_messages.push_back(std::move(fragment));
        remaining_bytes -= chunk_size;
    }

    return fragmented_messages;
}
