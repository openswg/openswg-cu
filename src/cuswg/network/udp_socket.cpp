
#include "udp_socket.h"

using boost::asio::buffer;
using boost::asio::io_service;
using boost::asio::ip::udp;
using boost::system::error_code;

using namespace cuswg;

UdpSocket::UdpSocket(io_service& io, uint32_t max_receive_size)
    : bytes_received_(0)
    , bytes_sent_(0)
    , max_receive_size_(max_receive_size)
    , receive_buffer_(max_receive_size)
    , socket_(io) {}

void UdpSocket::startListening(const Endpoint& listen_endpoint, ReceiveHandler&& handler)
{
    listen_endpoint_ = listen_endpoint;
    receive_handler_.swap(handler);

    socket_.open(udp::v4());
    socket_.bind(listen_endpoint_);

    asyncReceiveLoop();
}

void UdpSocket::stopListening()
{
    socket_.close();
}

void UdpSocket::sendTo(const ByteBuffer& send_buffer, const Endpoint& endpoint)
{
    socket_.async_send_to(boost::asio::buffer(send_buffer.c_data(), send_buffer.size()), endpoint,
        [this] (const error_code&, size_t bytes_transferred)
    {
        bytes_sent_ += bytes_transferred;
    });
}

void UdpSocket::asyncReceiveLoop()
{
    socket_.async_receive_from(buffer(receive_buffer_.data(), receive_buffer_.size()), receive_endpoint_,
        [this] (const error_code& error, size_t bytes_transferred)
    {
        // Call the receive handler only if a valid message was received
        if(!error && bytes_transferred > 0) {
            receive_handler_(ByteBuffer(receive_buffer_.data(), bytes_transferred), receive_endpoint_);

            bytes_received_ += bytes_transferred;
        }

        // Continue async reading until the work is aborted
        if(error != boost::asio::error::operation_aborted) {
            asyncReceiveLoop();
        }
    });
}
