
#include "connection_pool.h"
#include "connection.h"

using namespace cuswg;

Connection* ConnectionPool::find(const UdpSocket::Endpoint& endpoint) const
{    
    Connection* conn = nullptr;
    
    auto find_iter = connections_.find(endpoint);
    if(find_iter != connections_.end())
    {
        conn = find_iter->second.get();
    }

    return conn;
}

Connection* ConnectionPool::add(const UdpSocket::Endpoint& endpoint)
{    
    auto conn = find(endpoint);

    if(!conn)
    {
        std::cout << "Creating connection for: " << endpoint << std::endl;

        auto insert_iter = connections_.emplace(endpoint, std::unique_ptr<Connection>(new Connection(endpoint)));
        conn = insert_iter.first->second.get();
    }

    return conn;
}

bool ConnectionPool::remove(const UdpSocket::Endpoint& endpoint)
{
    return connections_.erase(endpoint) > 0;
}

bool ConnectionPool::remove(Connection* connection)
{
    return remove(connection->getEndpoint());
}
