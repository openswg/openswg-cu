
#pragma once

#include <list>
#include <memory>

#include <boost/asio.hpp>

#include "connection_pool.h"
#include "soe_protocol.h"
#include "udp_socket.h"
#include "cuswg/util/byte_buffer.h"

namespace cuswg {

class BaseSoeServer
{ 
public:
    explicit BaseSoeServer(boost::asio::io_service& io);

    void startListening(uint16_t listen_port);
    void stopListening();
    
    void sendMessage(Connection* connection, ByteBuffer& message, boost::optional<Connection::SequencedCallback> callback = boost::optional<Connection::SequencedCallback>());

    void disconnect(Connection* connection);
    
private:
    virtual void handleGameMessage(Connection* connection, ByteBuffer message) = 0;
    
    void handleProtocolMessage(Connection* connection, ByteBuffer message);
    void handleSessionRequest(UdpSocket::Endpoint remote_endpoint, ByteBuffer& message);
    void handleMultiPacket(Connection* connection, MultiPacket packet);
    void handleDisconnect(Connection* connection, Disconnect packet);
    void handlePing(Connection* connection, Ping packet);
    void handleNetStatsClient(Connection* connection, NetStatsClient packet);
    void handleChildDataA(Connection* connection, ChildDataA packet);
    void handleDataFragA(Connection* connection, DataFragA packet);
    void handleAckA(Connection* connection, AckA packet);
    void handleOutOfOrderA(Connection* connection, OutOfOrderA packet);

    void sendSessionResponse(Connection* connection);
    void sendDisconnect(Connection* connection, uint16_t reason = 6);
    void sendAck(Connection* connection, uint16_t sequence);
    void sendOutOfOrder(Connection* connection, uint16_t sequence);
    ByteBuffer sendProtocolMessage(Connection* connection, ByteBuffer message);
    void sendFragmentedMessage(Connection* connection, ByteBuffer& message, boost::optional<Connection::SequencedCallback> callback);
    void sendSequencedMessage(Connection* connection, ProtocolOpcodes header, ByteBuffer& message, boost::optional<Connection::SequencedCallback> callback);
    
    bool acknowledgeSequence(Connection* connection, uint16_t sequence);

    void compress(Connection* connection, ByteBuffer& message);
	void decompress(Connection* connection, ByteBuffer& message);

    void decrypt(Connection* connection, ByteBuffer& message);
    void encrypt(Connection* connection, ByteBuffer& message);

    void testCrc(Connection* connection, ByteBuffer& message);
    void addCrc(Connection* connection, ByteBuffer& message);
    
    std::list<ByteBuffer> splitDataChannelMessage(ByteBuffer message, uint32_t max_size);

    boost::asio::strand io_strand_;
    UdpSocket socket_;
    ConnectionPool connections_;
    std::vector<uint8_t> compression_buffer_;
    std::vector<uint8_t> decompression_buffer_;
    NetStatsServer server_net_stats_;
};

}  // namespace cuswg
