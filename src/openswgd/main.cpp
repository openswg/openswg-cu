
#include <iostream>
#include <thread>

#include <boost/asio.hpp>
#include <boost/optional.hpp>

#include "cuswg/login/login_server.h"

int main()
{
    boost::asio::io_service io;
    cuswg::LoginServer login_server(io);
    
    boost::optional<boost::asio::io_service::work> work(boost::in_place(std::ref(io)));
    
    std::thread t([&io] () {
        io.run();
    });
    
    login_server.startListening(44453);
    
    std::cout << "Starting server" << std::endl;

    for(;;)
    {
        std::string cmd;
        std::cin >> cmd;
        
        // Check for the exit command.
        if (cmd.compare("exit") == 0 || cmd.compare("quit") == 0 || cmd.compare("q") == 0)
        {
        	std::cout << "Stopping server" << std::endl;
        	break;
        }
        else 
        {
            // Log the invalid command.
            std::cout << "Command not found" << std::endl;
        }
    }

    login_server.stopListening();
    
    
    // thread cleanup
    work.reset();
    t.join();

    return 0;
}
