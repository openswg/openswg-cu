
include(GroupSourcesByFilesystem)

file(GLOB_RECURSE SOURCES *.h *.hpp *.cpp *.cc)

GroupSourcesByFilesystem("${SOURCES}")

add_executable(tre_archiver ${SOURCES})

target_link_libraries(tre_archiver
    cuswg
    ${Boost_LIBRARIES}
)

set_target_properties(tre_archiver PROPERTIES FOLDER examples)

if(ZLIB_LIBRARY_DEBUG)
    target_link_libraries(tre_archiver debug ${ZLIB_LIBRARY_DEBUG})
    get_filename_component(ZLIB_PATH ${ZLIB_LIBRARY_DEBUG} PATH)
    get_filename_component(ZLIB_FILE ${ZLIB_LIBRARY_DEBUG} NAME_WE)
endif()

if(ZLIB_LIBRARY_RELEASE)
    target_link_libraries(tre_archiver optimized ${ZLIB_LIBRARY_RELEASE})
    get_filename_component(ZLIB_PATH ${ZLIB_LIBRARY_DEBUG} PATH)
    get_filename_component(ZLIB_FILE ${ZLIB_LIBRARY_DEBUG} NAME_WE)
endif()

if(WIN32)
    add_custom_command(TARGET tre_archiver POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
            ${ZLIB_PATH}/${ZLIB_FILE}.dll
            $<TARGET_FILE_DIR:tre_archiver>)
endif()
